#
# Yuri Chornoivan <yurchor@ukr.net>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-develop-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:19+0000\n"
"PO-Revision-Date: 2023-01-17 09:21+0200\n"
"Last-Translator: Fracture dept <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: content/hig/_index.md:0
msgid "KDE Human Interface Guidelines"
msgstr "Настанови щодо зручності інтерфейсу KDE"

#: content/hig/_index.md:9
msgid ""
"The **KDE Human Interface Guidelines (HIG)** offer designers and developers "
"a set of recommendations for producing beautiful, usable, and consistent "
"user interfaces for convergent desktop and mobile applications and workspace "
"widgets. Our aim is to improve the experience for users by making consistent "
"interfaces for desktop, mobile, and everything in between, more consistent, "
"intuitive and learnable."
msgstr ""
"**Настанови щодо зручності інтерфейсу KDE (HIG)** пропонують дизайнерам та "
"розробникам набір рекомендацій щодо створення красивого, придатного до "
"користування та сумісного інтерфейсу користувача для забезпечення "
"однорідності програм та віджетів робочого простору для робочих станцій та "
"мобільних пристроїв. Метою є полегшення роботи користувачам шляхом створення "
"однорідніших, а отже інтуїтивніших та простіших у вивченні, інтерфейсів для "
"комп'ютерів, мобільних пристроїв та усіх проміжних пристроїв."

#: content/hig/_index.md:15
msgid "Design Vision"
msgstr "Бачення дизайну"

#: content/hig/_index.md:18
msgid ""
"Our design vision focuses on two attributes of KDE software that connect its "
"future to its history:"
msgstr ""
"Наше бачення дизайну сконцентровано на двох властивостях програмного "
"забезпечення KDE, які поєднують його майбутнє із його історією:"

#: content/hig/_index.md:21
msgid ""
"![Simple by default, powerful when needed.](/hig/HIGDesignVisionFullBleed."
"png)"
msgstr ""
"![Типово проста, але потужна, якщо потрібно.](/hig/HIGDesignVisionFullBleed."
"png)"

#: content/hig/_index.md:23
msgid "Simple by default..."
msgstr "Типово проста…"

#: content/hig/_index.md:25
msgid ""
"*Simple and inviting. KDE software is pleasant to experience and easy to use."
"*"
msgstr ""
"*Просте і гостинне. Програмне забезпечення KDE є приємним у роботі та "
"простим у користуванні.*"

#: content/hig/_index.md:28
msgid "**Make it easy to focus on what matters**"
msgstr "**Спрощення концентрації на важливих речах**"

#: content/hig/_index.md:30
msgid ""
"Remove or minimize elements not crucial to the primary or main task. Use "
"spacing to keep things organized. Use color to draw attention. Reveal "
"additional information or optional functions only when needed."
msgstr ""
"Вилучайте або мінімізуйте елементи, які не є критичними для первинної або "
"основної задачі. Користуйтеся інтервалами для упорядковування елементів. "
"Користуйтеся кольорами для привернення уваги. Показуйте додаткові відомості "
"або необов'язкові функціональні елементи лише за потреби."

#: content/hig/_index.md:35
msgid "**\"I know how to do that!\"**"
msgstr "**Я знаю, як це зробити!**"

#: content/hig/_index.md:37
msgid ""
"Make things easier to learn by reusing design patterns from other "
"applications. Other applications that use good design are a precedent to "
"follow."
msgstr ""
"Спрощуйте вивчення можливостей програми повторним використанням шаблонів "
"дизайну з інших програм. Варто дотримуватися дизайну інших програм, якщо цей "
"дизайн визнано добрим."

#: content/hig/_index.md:41
msgid "**Do the heavy lifting for me**"
msgstr "**Виконання важких завдань за користувача**"

#: content/hig/_index.md:43
msgid ""
"Make complex tasks simple. Make novices feel like experts. Create ways in "
"which your users can naturally feel empowered by your software."
msgstr ""
"Робіть складні завдання простішими. Дайте новим користувачам відчути себе "
"експертами. Робіть усе так, щоб користувачі відчували розширення своїх "
"можливостей, коли користуються вашим програмним забезпеченням."

#: content/hig/_index.md:47
msgid "...Powerful when needed"
msgstr "…Потужна там, де це потрібно"

#: content/hig/_index.md:49
msgid ""
"*Power and flexibility. KDE software allows users to be effortlessly "
"creative and efficiently productive.*"
msgstr ""
"*Потужність і гнучкість. Програмне забезпечення KDE надає користувачам змогу "
"творити без зайвих зусиль і бути ефективними.*"

#: content/hig/_index.md:52
msgid "**Solve a problem**"
msgstr "**Вирішення проблеми**"

#: content/hig/_index.md:54
msgid ""
"Identify and make very clear to the user what need is addressed and how."
msgstr ""
"Визначте і зробіть абсолютно зрозумілим користувачеві те, що саме слід "
"зробити і як."

#: content/hig/_index.md:57
msgid "**Always in control**"
msgstr "**Завжди під контролем**"

#: content/hig/_index.md:59
msgid ""
"It should always be clear what can be done, what is currently happening, and "
"what has just happened. The user should never feel at the mercy of the tool. "
"Give the user the final say."
msgstr ""
"Користувач завжди має чітко розуміти, що можна зробити, що відбувається і що "
"щойно відбулося. Користувач ніколи не повинен відчувати всевладність "
"програми. Дайте користувачеві простір для остаточних рішень."

#: content/hig/_index.md:64
msgid "**Be flexible**"
msgstr "**Гнучкість**"

#: content/hig/_index.md:66
msgid ""
"Provide sensible defaults but consider optional functionality and "
"customization options that don\\'t interfere with the primary task."
msgstr ""
"Надайте притомні типові значення параметрів, але забезпечте додаткові "
"параметри функціональних можливостей та налаштовування, які не заважатимуть "
"виконанню основного завдання програми."

#: content/hig/_index.md:70
msgid "Note"
msgstr "Примітка"

#: content/hig/_index.md:72
msgid ""
"KDE encourages developing and designing for customization, while providing "
"good default settings. Integrating into other desktop environments is also a "
"virtue, but ultimately we aim for perfection within our own Plasma desktop "
"environment with the default themes and settings. This aim should not be "
"compromised."
msgstr ""
"KDE заохочує розробку і створення дизайнів зі можливостями налаштовування, "
"одночасно надаючи у розпорядження користувачів добрі типові значення "
"параметрів. Вартою уваги є інтеграція із іншими стільничними середовищами, "
"але основний акцент ми робимо на ідеальній інтеграції із нашим стільничним "
"середовищем Плазми із типовими темами та параметрами. Такою інтеграцією не "
"можна нехтувати."
